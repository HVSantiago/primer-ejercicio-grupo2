﻿using System;

namespace EjercicioUnis1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables
            int bandera = 1;
            int contador = 0;
            int i = 0;
            int numero = 0;

            
            
            while (bandera != 0)
            {
                try
                {
                    Console.WriteLine("");

                    Console.WriteLine("UNIVERSIDAD DEL ISTMO DE GUATEMALA");
                    Console.WriteLine("GRUPO NO. 2");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    //Pedimos al usuario un numero.
                    Console.WriteLine("INGRESE CUALQUIER NUMERO");
                    numero = Convert.ToInt32(Console.ReadLine());

                    //Entramos al ciclo
                    for (i = 1; i <= (numero + 1); i++)
                    {
                        if (numero % i == 0)
                        {
                            contador++;
                        }
                    }
                    if (contador != 2)
                    {
                        Console.WriteLine("");
                        Console.WriteLine("El numero que ingresaste No es Primo");
                        Console.WriteLine("");
                        
                    }
                    else
                    {
                        Console.WriteLine("");
                        Console.WriteLine("El numero que ingresaste si es Primo");
                        Console.WriteLine("");
                        
                    }
                }
                catch
                {
                    Console.WriteLine("HA COMETIDO UN ERROR, INTENTALO DE NUEVO");
                    Console.Beep();
                    Console.WriteLine("");
                    
                }
                try
                {
                    Console.WriteLine("Ingrese 0 para salir del programa, o cualquier numero para continuar.");
                    bandera = Convert.ToInt32(Console.ReadLine());
                    
                }
                catch
                {
                    Console.WriteLine("HA COMETIDO UN ERROR, INTENTALO DE NUEVO");
                    Console.Beep();
                    Console.WriteLine("");
                    
                }
            }
        }
    }
}

                   
        
    